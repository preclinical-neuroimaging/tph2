TPH2 KO statistics
================
Alex Meng, Joanes Grandjean

# Setup environement

Download DABEST

``` r
# you just need to run these once. 

install.packages('devtools')
install.pacakges("reshape2")
install.pacakges("wesanderson")
install.packages("svglite")
install.packages("ggpubr")
devtools::install_github("ACCLAB/dabestr")   
devtools::install_github("karthik/wesanderson")
```

# Load pacakges

``` r
library(tidyverse)
library(glue)
library(dabestr)
library(wesanderson)  # see https://github.com/karthik/wesanderson
library(reshape2)
library(svglite)
library(ggpubr)
pal <- wes_palette("Darjeeling1")
```

###behavioural tests EPM (Fig1) ##Fig1 dataset description: A. Open arms
duration B. Number of entry into closed arms C. Latency time for first
time entering open arms D. Total distance moved

#Fig1A

``` r
df <- read_csv('assets/tables/EPM_open_arms_duration.csv', col_types = cols()) %>% melt() %>% dplyr::rename(group = variable) %>% drop_na()

df %>% group_by(group) %>% summarise(mean=round(mean(value),2), sd=round(sd(value),2))

p_tmp<- c(t.test(df$value[df$group == 'Tph2+/+'], df$value[df$group == 'Tph2+/-'],var.equal=TRUE)$p.value, t.test(df$value[df$group == 'Tph2+/+'], df$value[df$group == 'Tph2-/-'],var.equal=TRUE)$p.value)

dabest_hedges <- dabest(df, group, value, idx = c('Tph2+/+','Tph2+/-','Tph2-/-'), paired = FALSE)  %>% hedges_g() 
dabest_hedges

Fig1A<-plot(dabest_hedges, palette = pal,  rawplot.ylabel = "duration in open arms (%)") 
ggsave('assets/figure/Fig1A.svg', plot = Fig1A, device = 'svg',dpi = 300)

Fig1A

dabest_hedges$result %>% mutate(p = p_tmp)
```

#Fig1B

``` r
df <- read_csv('assets/tables/EPM_entry_closed_arms.csv', col_types = cols()) %>% melt() %>% dplyr::rename(group = variable) %>% drop_na()

df %>% group_by(group) %>% summarise(mean=round(mean(value),2), sd=round(sd(value),2))

p_tmp<- c(t.test(df$value[df$group == 'Tph2+/+'], df$value[df$group == 'Tph2+/-'],var.equal=TRUE)$p.value, t.test(df$value[df$group == 'Tph2+/+'], df$value[df$group == 'Tph2-/-'],var.equal=TRUE)$p.value)

dabest_hedges <- dabest(df, group, value, idx = c('Tph2+/+','Tph2+/-','Tph2-/-'), paired = FALSE)  %>% hedges_g() 
dabest_hedges

Fig1B<-plot(dabest_hedges, palette = pal,  rawplot.ylabel = "number of entry") 
ggsave('assets/figure/Fig1B.svg', plot = Fig1B, device = 'svg',dpi = 300)

Fig1B

dabest_hedges$result %>% mutate(p = p_tmp)
```

#Fig1C

``` r
df <- read_csv('assets/tables/EPM_first_time_entry.csv', col_types = cols()) %>% melt() %>% dplyr::rename(group = variable) %>% drop_na()

df %>% group_by(group) %>% summarise(mean=round(mean(value),2), sd=round(sd(value),2))

p_tmp<- c(t.test(df$value[df$group == 'Tph2+/+'], df$value[df$group == 'Tph2+/-'],var.equal=TRUE)$p.value, t.test(df$value[df$group == 'Tph2+/+'], df$value[df$group == 'Tph2-/-'],var.equal=TRUE)$p.value)

dabest_hedges <- dabest(df, group, value, idx = c('Tph2+/+','Tph2+/-','Tph2-/-'), paired = FALSE)  %>% hedges_g() 
dabest_hedges

Fig1C<-plot(dabest_hedges, palette = pal,  rawplot.ylabel = "latency time (s)") 
ggsave('assets/figure/Fig1C.svg', plot = Fig1C, device = 'svg',dpi = 300)

Fig1C

dabest_hedges$result %>% mutate(p = p_tmp)
```

#Fig1D

``` r
df <- read_csv('assets/tables/EPM_total_distance.csv', col_types = cols()) %>% melt() %>% dplyr::rename(group = variable) %>% drop_na()

df %>% group_by(group) %>% summarise(mean=round(mean(value),2), sd=round(sd(value),2))

p_tmp<- c(t.test(df$value[df$group == 'Tph2+/+'], df$value[df$group == 'Tph2+/-'],var.equal=TRUE)$p.value, t.test(df$value[df$group == 'Tph2+/+'], df$value[df$group == 'Tph2-/-'],var.equal=TRUE)$p.value)

dabest_hedges <- dabest(df, group, value, idx = c('Tph2+/+','Tph2+/-','Tph2-/-'), paired = FALSE)  %>% hedges_g() 
dabest_hedges

Fig1D<-plot(dabest_hedges, palette = pal,  rawplot.ylabel = "total distance moved (cm)") 
ggsave('assets/figure/Fig1D.svg', plot = Fig1D, device = 'svg',dpi = 300)

Fig1D

dabest_hedges$result %>% mutate(p = p_tmp)
```

###behavioural tests Social interaction (Fig2) ##Fig2 dataset
description: A. Experimental diagram B. Total no contact C. Total
mounting D. Total aggressiveness

#Fig2B

``` r
df <- read_csv('assets/tables/SI_total_no_contact.csv', col_types = cols()) %>% melt() %>% dplyr::rename(group = variable) %>% drop_na()

df %>% group_by(group) %>% summarise(mean=round(mean(value),2), sd=round(sd(value),2))

p_tmp<- c(t.test(df$value[df$group == 'Tph2+/+'], df$value[df$group == 'Tph2+/-'],var.equal=TRUE)$p.value, t.test(df$value[df$group == 'Tph2+/+'], df$value[df$group == 'Tph2-/-'],var.equal=TRUE)$p.value)

dabest_hedges <- dabest(df, group, value, idx = c('Tph2+/+','Tph2+/-','Tph2-/-'), paired = FALSE)  %>% hedges_g() 
dabest_hedges

Fig2B<-plot(dabest_hedges, palette = pal,  rawplot.ylabel = "total no contact (%)") 
ggsave('assets/figure/Fig2B.svg', plot = Fig2B, device = 'svg',dpi = 300)

Fig2B

dabest_hedges$result %>% mutate(p = p_tmp)
```

#Fig2C

``` r
df <- read_csv('assets/tables/SI_total_mounting.csv', col_types = cols()) %>% melt() %>% dplyr::rename(group = variable) %>% drop_na()

df %>% group_by(group) %>% summarise(mean=round(mean(value),2), sd=round(sd(value),2))

p_tmp<- c(t.test(df$value[df$group == 'Tph2+/+'], df$value[df$group == 'Tph2+/-'],var.equal=TRUE)$p.value, t.test(df$value[df$group == 'Tph2+/+'], df$value[df$group == 'Tph2-/-'],var.equal=TRUE)$p.value)

dabest_hedges <- dabest(df, group, value, idx = c('Tph2+/+','Tph2+/-','Tph2-/-'), paired = FALSE)  %>% hedges_g() 
dabest_hedges

Fig2C<-plot(dabest_hedges, palette = pal,  rawplot.ylabel = "total mounting (%)") 
ggsave('assets/figure/Fig2C.svg', plot = Fig2C, device = 'svg',dpi = 300)

Fig2C

dabest_hedges$result %>% mutate(p = p_tmp)
```

#Fig2D

``` r
df <- read_csv('assets/tables/SI_total_aggressiveness.csv', col_types = cols()) %>% melt() %>% dplyr::rename(group = variable) %>% drop_na()

df %>% group_by(group) %>% summarise(mean=round(mean(value),2), sd=round(sd(value),2))

p_tmp<- c(t.test(df$value[df$group == 'Tph2+/+'], df$value[df$group == 'Tph2+/-'],var.equal=TRUE)$p.value, t.test(df$value[df$group == 'Tph2+/+'], df$value[df$group == 'Tph2-/-'],var.equal=TRUE)$p.value)

dabest_hedges <- dabest(df, group, value, idx = c('Tph2+/+','Tph2+/-','Tph2-/-'), paired = FALSE)  %>% hedges_g() 
dabest_hedges

Fig2D<-plot(dabest_hedges, palette = pal,  rawplot.ylabel = "total aggressiveness (%)") 
ggsave('assets/figure/Fig2D.svg', plot = Fig2D, device = 'svg',dpi = 300)

Fig2D

dabest_hedges$result %>% mutate(p = p_tmp)
```

###Expression of oxytocin & receptors (Fig3) ##Fig3 dataset description:
A: Diagram of brain punching position B. Prelimbic cortex C. Ventral CA1
region of hippocampus D. Ventral CA3 region of hippocampus E. Dorsal
raphe nucleus F. Medial frontal cortex G. Hippocampus H. Paraventricular
thalamic nucleus I. Central amygdala nucleus

#Fig3B

``` r
df <- read_csv('assets/tables/PCR_PRL.csv', col_types = cols()) %>% melt() %>% dplyr::rename(group = variable) %>% drop_na()

df %>% group_by(group) %>% summarise(mean=round(mean(value),2), sd=round(sd(value),2))

dabest_hedges <- dabest(df, group, value, idx = c('Tph2+/+','Tph2-/-'), paired = FALSE)  %>% hedges_g() 

dabest_hedges

plot(dabest_hedges, palette = pal)

Fig3B <- plot(dabest_hedges, palette = pal, rawplot.ylabel = "mRNA level (% vs Tph2+/+)")

p_tmp<- c(t.test(df$value[df$group == 'Tph2+/+'], df$value[df$group == 'Tph2-/-'],var.equal=TRUE)$p.value)

dabest_hedges$result %>% mutate(p = p_tmp)

ggsave('assets/figure/Fig3B.svg', plot = Fig3B, device = 'svg',dpi = 300)
```

#Fig3C

``` r
df <- read_csv('assets/tables/PCR_vCA1.csv', col_types = cols()) %>% melt() %>% dplyr::rename(group = variable) %>% drop_na()

df %>% group_by(group) %>% summarise(mean=round(mean(value),2), sd=round(sd(value),2))

dabest_hedges <- dabest(df, group, value, idx = c('Tph2+/+','Tph2-/-'), paired = FALSE)  %>% hedges_g() 

dabest_hedges

plot(dabest_hedges, palette = pal)

Fig3C <- plot(dabest_hedges, palette = pal, rawplot.ylabel = "mRNA level (% vs Tph2+/+)")

p_tmp<- c(t.test(df$value[df$group == 'Tph2+/+'], df$value[df$group == 'Tph2-/-'],var.equal=TRUE)$p.value)

dabest_hedges$result %>% mutate(p = p_tmp)

ggsave('assets/figure/Fig3C.svg', plot = Fig3C, device = 'svg',dpi = 300)
```

#Fig3D

``` r
df <- read_csv('assets/tables/PCR_vCA3.csv', col_types = cols()) %>% melt() %>% dplyr::rename(group = variable) %>% drop_na()

df %>% group_by(group) %>% summarise(mean=round(mean(value),2), sd=round(sd(value),2))

dabest_hedges <- dabest(df, group, value, idx = c('Tph2+/+','Tph2-/-'), paired = FALSE)  %>% hedges_g() 

dabest_hedges

plot(dabest_hedges, palette = pal)

Fig3D <- plot(dabest_hedges, palette = pal, rawplot.ylabel = "mRNA level (% vs Tph2+/+)")

p_tmp<- c(t.test(df$value[df$group == 'Tph2+/+'], df$value[df$group == 'Tph2-/-'],var.equal=TRUE)$p.value)

dabest_hedges$result %>% mutate(p = p_tmp)

ggsave('assets/figure/Fig3D.svg', plot = Fig3D, device = 'svg',dpi = 300)
```

#Fig3E

``` r
df <- read_csv('assets/tables/PCR_DR.csv', col_types = cols()) %>% melt() %>% dplyr::rename(group = variable) %>% drop_na()

df %>% group_by(group) %>% summarise(mean=round(mean(value),2), sd=round(sd(value),2))

dabest_hedges <- dabest(df, group, value, idx = c('Tph2+/+','Tph2-/-'), paired = FALSE)  %>% hedges_g() 

dabest_hedges

plot(dabest_hedges, palette = pal)

Fig3E <- plot(dabest_hedges, palette = pal, rawplot.ylabel = "mRNA level (% vs Tph2+/+)")

p_tmp<- c(t.test(df$value[df$group == 'Tph2+/+'], df$value[df$group == 'Tph2-/-'],var.equal=TRUE)$p.value)

dabest_hedges$result %>% mutate(p = p_tmp)

ggsave('assets/figure/Fig3E.svg', plot = Fig3E, device = 'svg',dpi = 300)
```

#Fig3F

``` r
df <- read_csv('assets/tables/OXY_MFC.csv', col_types = cols()) %>% melt() %>% dplyr::rename(group = variable) %>% drop_na()

df %>% group_by(group) %>% summarise(mean=round(mean(value),2), sd=round(sd(value),2))

dabest_hedges <- dabest(df, group, value, idx = c('Tph2+/+','Tph2-/-'), paired = FALSE)  %>% hedges_g() 

dabest_hedges

plot(dabest_hedges, palette = pal)

Fig3F <- plot(dabest_hedges, palette = pal, rawplot.ylabel = "pg oxytocin/µg tissue protein/ml")

p_tmp<- c(t.test(df$value[df$group == 'Tph2+/+'], df$value[df$group == 'Tph2-/-'],var.equal=TRUE)$p.value)

dabest_hedges$result %>% mutate(p = p_tmp)

ggsave('assets/figure/Fig3F.svg', plot = Fig3F, device = 'svg',dpi = 300)
```

#Fig3G

``` r
df <- read_csv('assets/tables/OXY_HIP.csv', col_types = cols()) %>% melt() %>% dplyr::rename(group = variable) %>% drop_na()

df %>% group_by(group) %>% summarise(mean=round(mean(value),2), sd=round(sd(value),2))

dabest_hedges <- dabest(df, group, value, idx = c('Tph2+/+','Tph2-/-'), paired = FALSE)  %>% hedges_g() 

dabest_hedges

plot(dabest_hedges, palette = pal)

Fig3G <- plot(dabest_hedges, palette = pal, rawplot.ylabel = "pg oxytocin/µg tissue protein/ml")

p_tmp<- c(t.test(df$value[df$group == 'Tph2+/+'], df$value[df$group == 'Tph2-/-'],var.equal=TRUE)$p.value)

dabest_hedges$result %>% mutate(p = p_tmp)

ggsave('assets/figure/Fig3G.svg', plot = Fig3G, device = 'svg',dpi = 300)
```

#Fig3H

``` r
df <- read_csv('assets/tables/OXY_PVN.csv', col_types = cols()) %>% melt() %>% dplyr::rename(group = variable) %>% drop_na()

df %>% group_by(group) %>% summarise(mean=round(mean(value),2), sd=round(sd(value),2))

dabest_hedges <- dabest(df, group, value, idx = c('Tph2+/+','Tph2-/-'), paired = FALSE)  %>% hedges_g() 

dabest_hedges

plot(dabest_hedges, palette = pal)

Fig3H <- plot(dabest_hedges, palette = pal, rawplot.ylabel = "pg oxytocin/µg tissue protein/ml")

p_tmp<- c(t.test(df$value[df$group == 'Tph2+/+'], df$value[df$group == 'Tph2-/-'],var.equal=TRUE)$p.value)

dabest_hedges$result %>% mutate(p = p_tmp)

ggsave('assets/figure/Fig3H.svg', plot = Fig3H, device = 'svg',dpi = 300)
```

#Fig3I

``` r
df <- read_csv('assets/tables/OXY_CA.csv', col_types = cols()) %>% melt() %>% dplyr::rename(group = variable) %>% drop_na()

df %>% group_by(group) %>% summarise(mean=round(mean(value),2), sd=round(sd(value),2))

dabest_hedges <- dabest(df, group, value, idx = c('Tph2+/+','Tph2-/-'), paired = FALSE)  %>% hedges_g() 

dabest_hedges

plot(dabest_hedges, palette = pal)

Fig3I <- plot(dabest_hedges, palette = pal, rawplot.ylabel = "pg oxytocin/µg tissue protein/ml")

p_tmp<- c(t.test(df$value[df$group == 'Tph2+/+'], df$value[df$group == 'Tph2-/-'],var.equal=TRUE)$p.value)

dabest_hedges$result %>% mutate(p = p_tmp)

ggsave('assets/figure/Fig3I.svg', plot = Fig3I, device = 'svg',dpi = 300)
```
